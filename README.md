# SIGE
 
## Repositório
 
Este é o repositório da organização `Sige-2021-2` da disciplina Gerência de Configuração e Evolução de Software. O propósito desse repositório é armazenar todos os documentos de `Sprints`, padrões de projeto, métricas e reuniões que serão gerados ao longo do semestre, além disso, também serve para armazenar as `issues` que são desenvolvidas de acordo com a necessidade do projeto.
 
## Objetivo
 
O objetivo da disciplina Gerência de Configuração e Evolução de Software (GCES) é introduzir ao aluno diversas técnicas de boa prática de como contribuir com códigos de terceiros e ensinar ao aluno a aplicação de técnicas voltadas a cultura de `DevOps`, tais quais como:
- Identificação de configuração.
- Controle de mudanças e versões.
- Integração e entrega contínua.
- Monitoramento do desenvolvimento de software.
- Gerenciamento do processo de construção/build.
- Pacotes e dependência de software.
- Princípios e técnicas de manutenção de software.
- Sustentação de software
 
## Projeto
 
O Sistema de Gestão Energética - Universidade de Brasília (SIGE-UnB), consiste em uma aplicação web desenvolvida para auxiliar o monitoramento e gestão das instalações elétricas das edificações dos campi da Universidade de Brasília. A ideia visa que cada Campus contido na UnB possa ser capaz de utilizar a ferramenta e ter um maior conhecimento de como está o consumo de seus insumos importantes ao longo do tempo, possibilitando, assim, elaborar possíveis estratégias que auxiliem em sua economia. Para mais informações sobre o projeto acesse a documentação do [SIGE - Sistema de Gestão Energética
](https://gitlab.com/lappis-unb/projects/SMI/docs).
 
## Alunos
 
||Nome|Email|
|:---:|:---:|:---:|
|<img width="150" height="150" src="./assets/team_photos/andre.jpeg">|André Lucas Ferreira Lemos de Souza|andre.lucas.lemos@hotmail.com|
|<img width="150" height="150" src="./assets/team_photos/ruan.jpeg"/>|Antonio Ruan Moura Barreto|ruanmoura13@outlook.com|
|<img width="150" height="150" src="./assets/team_photos/danillo.jpeg"/>|Danillo Goncalves de Souza|danillosouza1704@gmail.com|
|<img width="150" height="150" src="./assets/team_photos/darmacone.jpeg"/>|Damarcones dos Santos Duque Porto|damarcones@gmail.com|
|<img width="150" height="150" src="./assets/team_photos/flavio.jpeg"/>|Flavio Vieira Leao|flavio.vl@gmail.com|
|<img width="150" height="150" src="./assets/team_photos/francisco.jpeg"/>|Francisco Emanoel Ferreira|francisco1code@gmail.com|
|<img width="150" height="150" src="./assets/team_photos/herick.jpeg"/>|Herick Ferreira de Souza Portugues|herick.portugues@gmail.com|
|<img width="150" height="150" src="./assets/team_photos/vitor.jpeg"/>|João Vitor Ferreira Alves|vitor.alves07750@outlook.com|
|<img width="150" height="150" src="./assets/team_photos/leonardo.jpeg"/>|Leonardo da Silva Gomes|leonardodasigomes@gmail.com|
|<img width="150" height="150" src="./assets/team_photos/lucas.jpeg"/>|Lucas Melo dos Santos|lucasmelodos322@gmail.com|
|<img width="150" height="150" src="./assets/team_photos/matheus.jpeg"/>|Matheus de Cristo Doreia Estanislau|matheus.estanislau7@gmail.com|
|<img width="150" height="150" src="./assets/team_photos/nilo.jpeg"/>|Nilo Mendonça de Brito|nilojrmendonca@gmail.com|
|<img width="150" height="150" src="./assets/team_photos/pedro.jpeg"/>|Pedro Henrique Vieira|pedrohlima20@gmail.com|
|<img width="150" height="150" src="./assets/team_photos/rodrigo.jpeg"/>|Rodrigo Tiago Costa Lima|rodrigotiagocl@gmail.com|
 

