## Documentação da Sprint Planning Nº XYZ

### Timebox
| Descrição | Data              |
| :-------- | :-----------------|
| **Data de início**  | (data)  |
| **Data de término** | (data)  |
| **Duração**         | XZ Dias |

### Tarefas da sprint

| Issue | Responsável | Revisor | Concluída | Causa |
| :-    | :-          | :-      | :-        | :-    | 
| #(id da issue) | @(nome do responsável) | @(nome do revisor) | | |
| #(id da issue) | @(nome do responsável) | @(nome do revisor) | | |
| #(id da issue) | @(nome do responsável) | @(nome do revisor) | | |

<!-- ✔️✖️⚠️ -->
<!-- @LeoSilvaGomes --> 
<!-- @DanilloGS --> 
<!-- @francisco1code  --> 