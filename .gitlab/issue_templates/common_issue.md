<!-- # Nome da Issue -->

<!-- Referência de issue no repositório principal -->

## Descrição
<!-- Descrição detalhada do que deverá ser feito nessa issue -->

### Critérios de Aceitação:
<!-- Liste aqui o que o revisor terá que verificar para determinar se a issue foi ou não concluída -->

### Tarefas:
<!-- Lista de tarefas que o responsável por essa issue terá que fazer -->

- [ ] Tarefa 1
- [ ] Tarefa 2
- [ ] Tarefa 3

### Referência <!-- optativa -->