# Resultados

Esse documento tem como objetivo mostrar todas as issues feitas ao redor do projeto SiGE no segundo semestre de 2021. Nesse periodo conseguimos progredir com muitas estruturas dentro do codigo e principalmente trazer um progresso de estrategias para o projeto. Umas das nossas principais conquistas são a refatoração do front e o melhoramento de algumas partes na documentação.

---

### Tarefas

**1. Disciplina**

- [3](https://gitlab.com/sige-2021-2/disciplina/-/issues/3) - Criar documentação a respeito do git flow
- [8](https://gitlab.com/sige-2021-2/disciplina/-/issues/8) - Criar documento sobre metodologia adotada pela equipe
- [16](https://gitlab.com/sige-2021-2/disciplina/-/issues/16) - Atualizar o README com as informações do grupo

**1. Documentação**

- [1](https://gitlab.com/sige-2021-2/disciplina/-/issues/1) - Atualizar lista de contribuidores
- [2](https://gitlab.com/sige-2021-2/disciplina/-/issues/2) - Criar organização do projeto
- [4](https://gitlab.com/sige-2021-2/disciplina/-/issues/4) - Atualizar documento de arquitetura do SIGE
- [9](https://gitlab.com/sige-2021-2/disciplina/-/issues/9) - Atualizar documentação First-Step nos projetos
- [36](https://gitlab.com/sige-2021-2/disciplina/-/issues/36) - Ajustar documentação referente ao mobile
- [37](https://gitlab.com/sige-2021-2/disciplina/-/issues/37) - Ajustar documentação referente ao mobile
- [40](https://gitlab.com/sige-2021-2/disciplina/-/issues/40) - Centralizar arquivos assets da documentação

**2. Web Frontend**

- [38](https://gitlab.com/sige-2021-2/disciplina/-/issues/38) - Apagar pasta assets no repositório front-web
- [27](https://gitlab.com/sige-2021-2/disciplina/-/issues/27) - Atualização da versão do node
- [10](https://gitlab.com/sige-2021-2/disciplina/-/issues/10) - Revisar merge request no web e mobile
- [12](https://gitlab.com/sige-2021-2/disciplina/-/issues/12) - Criar proposta de novo visual SIGE
- [21](https://gitlab.com/sige-2021-2/disciplina/-/issues/21) - Alterar resolução do gráfico
- [39](https://gitlab.com/sige-2021-2/disciplina/-/issues/39) - Ajustar eslint da plataforma
- [14](https://gitlab.com/sige-2021-2/disciplina/-/issues/14) - Adicionar Loader no gráfico após requisição de novo intervalo de tempo
- [15](https://gitlab.com/sige-2021-2/disciplina/-/issues/15) - Retirar informações extras que tornam o mapa poluído
- [35](https://gitlab.com/sige-2021-2/disciplina/-/issues/35) - Adicionar prettier na plataforma
- [22](https://gitlab.com/sige-2021-2/disciplina/-/issues/22) - Correção de valores nulos plotados no gráfico
- [48](https://gitlab.com/sige-2021-2/disciplina/-/issues/48) - Refatorar componente OccurenceList
- [52](https://gitlab.com/sige-2021-2/disciplina/-/issues/52) - Refatorar componente Occurences
- [51](https://gitlab.com/sige-2021-2/disciplina/-/issues/51) - Refatorar componente MapModal
- [53](https://gitlab.com/sige-2021-2/disciplina/-/issues/53) - Refatorar componente ChangeBarChart
- [56](https://gitlab.com/sige-2021-2/disciplina/-/issues/56) - Refatorar componente TriangularConsumptionChart
- [57](https://gitlab.com/sige-2021-2/disciplina/-/issues/57) - Refatorar componente GroupDialog
- [66](https://gitlab.com/sige-2021-2/disciplina/-/issues/66) - Refatorar dash bottom bar
- [63](https://gitlab.com/sige-2021-2/disciplina/-/issues/63) - Refatorar total cost filter
- [67](https://gitlab.com/sige-2021-2/disciplina/-/issues/67) - Refatorar dash campus tab
- [49](https://gitlab.com/sige-2021-2/disciplina/-/issues/49) - Refatorar componente MeasurementsBox
- [43](https://gitlab.com/sige-2021-2/disciplina/-/issues/43) - Refatorar componente Barchart
- [46](https://gitlab.com/sige-2021-2/disciplina/-/issues/46) - Refatorar componente DashLast72hCard
- [42](https://gitlab.com/sige-2021-2/disciplina/-/issues/42) - Refatorar componente DashEventCard
- [64](https://gitlab.com/sige-2021-2/disciplina/-/issues/64) - Refatorar componente ConsuptiomFilter
- [62](https://gitlab.com/sige-2021-2/disciplina/-/issues/62) - Refatorar componente DashChargeBarCard
- [59](https://gitlab.com/sige-2021-2/disciplina/-/issues/59) - Refatorar componente DashGeneralEventBar
- Refatorar componente DashLastMeasurementCard
- Refatorar componente ActiveBox

**3. Master**

- [23](https://gitlab.com/sige-2021-2/disciplina/-/issues/23) - Fazer com que a tabela de reports receba dados da api
- [28](https://gitlab.com/sige-2021-2/disciplina/-/issues/28) - Refatorar as comunicações Modbus usando a biblioteca PyModbus

### Considerações

Além disso deixamos algumas ideias para os proximos semestre em relação ao desenvolvimento, como algumas tasks de refatoração que podem ser aplicadas também ao mobile e o inicio de um prototipo para melhor o ux/ui da plataforma
