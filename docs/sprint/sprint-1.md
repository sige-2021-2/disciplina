# Sprint 1

Periodo da sprint 1

**Inicio:** 01/02/2022
**Fim:** 10/02/2022

---

### Issues Planejadas

**1. Disciplina**

- [3](https://gitlab.com/sige-2021-2/disciplina/-/issues/3) - Criar documentação a respeito do git flow
- [8](https://gitlab.com/sige-2021-2/disciplina/-/issues/8) - Criar documento sobre metodologia adotada pela equipe
- [16](https://gitlab.com/sige-2021-2/disciplina/-/issues/16) - Atualizar o README com as informações do grupo

**1. Documentação**

- [1](https://gitlab.com/sige-2021-2/disciplina/-/issues/1) - Atualizar lista de contribuidores
- [2](https://gitlab.com/sige-2021-2/disciplina/-/issues/2) - Criar organização do projeto
- [4](https://gitlab.com/sige-2021-2/disciplina/-/issues/4) - Atualizar documento de arquitetura do SIGE
- [9](https://gitlab.com/sige-2021-2/disciplina/-/issues/9) - Atualizar documentação First-Step nos projetos
- [36](https://gitlab.com/sige-2021-2/disciplina/-/issues/36) - Ajustar documentação referente ao mobile
- [37](https://gitlab.com/sige-2021-2/disciplina/-/issues/37) - Ajustar documentação referente ao mobile
- [40](https://gitlab.com/sige-2021-2/disciplina/-/issues/40) - Centralizar arquivos assets da documentação

**2. Web Frontend**

- [38](https://gitlab.com/sige-2021-2/disciplina/-/issues/38) - Apagar pasta assets no repositório front-web
- [27](https://gitlab.com/sige-2021-2/disciplina/-/issues/27) - Atualização da versão do node
- [10](https://gitlab.com/sige-2021-2/disciplina/-/issues/10) - Revisar merge request no web e mobile
- [21](https://gitlab.com/sige-2021-2/disciplina/-/issues/21) - Alterar resolução do gráfico
- [39](https://gitlab.com/sige-2021-2/disciplina/-/issues/39) - Ajustar eslint da plataforma
- [14](https://gitlab.com/sige-2021-2/disciplina/-/issues/14) - Adicionar Loader no gráfico após requisição de novo intervalo de tempo
- [15](https://gitlab.com/sige-2021-2/disciplina/-/issues/15) - Retirar informações extras que tornam o mapa poluído
- [35](https://gitlab.com/sige-2021-2/disciplina/-/issues/35) - Adicionar prettier na plataforma
- [22](https://gitlab.com/sige-2021-2/disciplina/-/issues/22) - Correção de valores nulos plotados no gráfico

**3. Master**

- [23](https://gitlab.com/sige-2021-2/disciplina/-/issues/23) - Fazer com que a tabela de reports receba dados da api
- [28](https://gitlab.com/sige-2021-2/disciplina/-/issues/28) - Refatorar as comunicações Modbus usando a biblioteca PyModbus

---

### Sprint Review

#### Issues concluidas

|                             ID                              |                                  Nome                                  |   Frente   |    Responsáveis    |
| :---------------------------------------------------------: | :--------------------------------------------------------------------: | :--------: | :----------------: |
|  [3](https://gitlab.com/sige-2021-2/disciplina/-/issues/3)  |               Criar documentação a respeito do git flow                |    Doc     |      Leonardo      |
|  [8](https://gitlab.com/sige-2021-2/disciplina/-/issues/8)  |         Criar documento sobre metodologia adotada pela equipe          | Disciplina |      Leonardo      |
|  [2](https://gitlab.com/sige-2021-2/disciplina/-/issues/2)  |                      Criar organização do projeto                      | Disciplina |      Danillo       |
|  [9](https://gitlab.com/sige-2021-2/disciplina/-/issues/9)  |             Atualizar documentação First-Step nos projetos             |    Doc     |       Pedro        |
| [16](https://gitlab.com/sige-2021-2/disciplina/-/issues/16) |             Atualizar o README com as informações do grupo             | Disciplina |      Danillo       |
| [39](https://gitlab.com/sige-2021-2/disciplina/-/issues/39) |                      Ajustar eslint da plataforma                      |   Front    | Leonardo e Matheus |
| [22](https://gitlab.com/sige-2021-2/disciplina/-/issues/22) |             Correção de valores nulos plotados no gráfico              |   Front    |   Herick e Pedro   |
| [21](https://gitlab.com/sige-2021-2/disciplina/-/issues/21) |                      Alterar resolução do gráfico                      |   Front    |    Nilo e Vitor    |
| [10](https://gitlab.com/sige-2021-2/disciplina/-/issues/10) |                 Revisar merge request no web e mobile                  |   Front    |      Leonardo      |
| [27](https://gitlab.com/sige-2021-2/disciplina/-/issues/27) |                     Atualização da versão do node                      |   Front    |      Danillo       |
| [40](https://gitlab.com/sige-2021-2/disciplina/-/issues/40) |              Centralizar arquivos assets da documentação               |    Doc     | Leonardo e Matheus |
| [38](https://gitlab.com/sige-2021-2/disciplina/-/issues/38) |              Apagar pasta assets no repositório front-web              |   Front    | Leonardo e Matheus |
| [37](https://gitlab.com/sige-2021-2/disciplina/-/issues/37) |                Ajustar documentação referente ao mobile                |    Doc     |      Leonardo      |
| [14](https://gitlab.com/sige-2021-2/disciplina/-/issues/14) | Adicionar Loader no gráfico após requisição de novo intervalo de tempo |   Front    |   André e Lucas    |

#### Débito técnico

|                             ID                              |                             Nome                              |    Frente    |   Responsáveis   |
| :---------------------------------------------------------: | :-----------------------------------------------------------: | :----------: | :--------------: |
| [23](https://gitlab.com/sige-2021-2/disciplina/-/issues/23) |     Fazer com que a tabela de reports receba dados da api     | Back - Front |       Nilo       |
| [28](https://gitlab.com/sige-2021-2/disciplina/-/issues/28) | Refatorar as comunicações Modbus usando a biblioteca PyModbus |     Back     | Flavio e Rodrigo |
| [15](https://gitlab.com/sige-2021-2/disciplina/-/issues/15) |     Retirar informações extras que tornam o mapa poluído      |    Front     |      Herick      |
| [35](https://gitlab.com/sige-2021-2/disciplina/-/issues/35) |               Adicionar prettier na plataforma                |    Front     |     Danillo      |
|  [1](https://gitlab.com/sige-2021-2/disciplina/-/issues/1)  |               Atualizar lista de contribuidores               |     Doc      |    Damarcones    |
|  [4](https://gitlab.com/sige-2021-2/disciplina/-/issues/4)  |          Atualizar documento de arquitetura do SIGE           |     Doc      |       Ruan       |
| [36](https://gitlab.com/sige-2021-2/disciplina/-/issues/36) |           Ajustar documentação referente ao mobile            |     Doc      |     Matheus      |

### Considerações

Nessa sprint foram priorizadas issues leves para que os alunos se ambientalizem com o projeto.

A maioria das issues que ficaram como divida tecnica já eram esperadas de serem grandes o suficientes para não caberem nessa sprint, entretanto houve essa priorização pois entende-se que deve haver um estudo previo para tais demandas!
