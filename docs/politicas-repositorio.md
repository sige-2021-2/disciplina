# <center> Políticas do Repositório
## Histórico de versão

| Data | Versão | Descrição | Autor|
| :-: | :-: | :-: | :-: |
| 02/02/2022 | 1.0 | Criação do Documento | Leonardo Gomes |

<div align="justify">

## 1. Introdução

Este documento tem como objetivo explicar os procedimentos a serem feitos para que as políticas deste repositório sejam seguidas adequadamente.

Aqui se encontram:

- Princípios básicos
- Gitflow
- Política de Branch
- Política de Commits

## 2. Princípios básicos

### 2.1 Faça commits atômicos

Sempre dividir o trabalho em pequenos e significativos commits fazendo com que cada commit implemente apenas uma funcionalidade.

### 2.2 Sempre escrever commits em INGLÊS

A fim de deixar o projeto mais acessível ao público global, o idioma padrão adotado tanto para o código quanto para tudo o que se relaciona diretamente ao mesmo é o inglês, logo as mensagens dos commits devem estar completamente em inglês.

### 3. Gitflow

O Gitflow é um modelo alternativo de ramificação do Git que consiste no uso de ramificações de recursos e várias ramificações primárias. Ele foi publicado pela primeira vez e popularizado por [Vincent Driessen no nvie](https://nvie.com/posts/a-successful-git-branching-model/).

Sobre a proposta ofertada pela matéria, o gitflow padrão teve que ser alterado para uma nova estrutura que comportasse a estrutura de branches de um projeto *forked*. Essa estrutura segue um conceito de *development mirrored*, ou seja, a branch *development* do projeto principal e do projeto *forked* devem sempre ser espelhas, assim garantindo a conformidade do projeto. Abaixo vc pode ver mais claramente como essa estrutura funciona:

![gitflow.png](./assets/gitflow.png)

As features branches devem sempre serem ramificadas a partir da development *forked* e abrir os pull request apontando para a development original, assim podendo inciar uma discussão com a comunidade sem interferir diretamente com o projeto original. O repositório original deve ficar **intacto**, não deve haver nenhuma nova branch. Em contra partida o repositório *forked* deve conter todas as branches e alterações futuras.

### 4. Políticas de Branch

Branches devem seguir as seguintes regras explicadas neste tópico:

Breve explicação sobre o fluxo de trabalho
A branch `main` representa uma versão estável do produto, contendo código já versionado.
Regras:

Existe apenas uma branch `main`.
Não são permitidos commits feitos diretamente na `main`.

As branches feature representam as entregas do projeto a serem desenvolvidas.

Regras:

- Essa branch sempre é criada a partir da branch `main`.
- Essa branch sempre é mesclada à branch `main`.
- Regras de nomenclatura:

```
issueID/titulo_da_issue
```

Essa issueID deve seguir o primeiramente o repositório principal do SIGE, se não houver issue que envolva a feature, deve se criar com o número de issue do repositório `Disciplina`

### 5. Políticas de Commits

Commits devem ser escritos de forma clara e breve, em inglês, descrevendo as alterações feitas.

Regras para escrita das mensagens nos commits:

```
#issueID - Mensagem breve descrevendo alterações
```

O caractere "#", por padrão, representa uma linha de comentário no arquivo de mensagem do commit. Para evitar problemas, é necessário alterar o caractere com o seguinte comando:

*git config --local core.commentChar auto*

Caso deseje utilizar um outro caractere específico para definir uma linha de comentário, basta substituir a palavra "auto" pelo caractere desejado.

A mensagem principal do commit deve ser escrita no **modo imperativo**. Aqui estão alguns exemplos:

Maus exemplos:

*Renamed the iVars and removed the common prefix.*

*Creating branch policies document*

Bons exemplos:

*Rename the iVars to remove the common prefix.*

*Create branch policies document*

---

## Bibliografia

- Gitflow Workflow. Disponível em https://www.atlassian.com/br/git/tutorials/comparing-workflows/gitflow-workflow
- Successful Git Branching Model. Disponível em https://nvie.com/posts/a-successful-git-branching-model/

</div>